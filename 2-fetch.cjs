const usersUrl = "https://jsonplaceholder.typicode.com/users";
const todosUrl = "https://jsonplaceholder.typicode.com/todos";

function fetchJson(url) {
    return fetch(url)
        .then((data) => data.json())
        .catch((err) => console.error(err));
}

//1. Fetch all the users
fetchJson(usersUrl)
    .then((response) => console.log(response))
    .catch((err) => console.error(err));

//2. Fetch all the todos
fetchJson(todosUrl)
    .then((response) => console.log(response))
    .catch((err) => console.error(err));

//3. Use the promise chain and fetch the users first and then the todos.
fetchJson(usersUrl)
    .then((responseUsers) => console.log(responseUsers))
    .then(() => fetchJson(todosUrl))
    .then((responseTodos) => console.log(responseTodos))
    .catch((err) => console.error(err));

//4. Use the promise chain and fetch the users first and then all the details for each user.
fetchJson(usersUrl)
    .then((users) => {
        let ids = users.map((user) => user.id);
        return ids;
    })
    .then((ids) => {
        return Promise.all(
            ids.map((id) => fetchJson(`https://jsonplaceholder.typicode.com/users?id=${id}`))
        );
    })
    .then((data) => console.log(data))
    .catch((err) => console.error(err));

//5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo.
fetchJson(todosUrl)
    .then((todos) => {
        let firstTodoUser = todos[0].userId;
        return todos.filter((todo)=>todo.userId==firstTodoUser);
    })
    .then((res) => console.log(res))
    .catch((err) => console.error(err))


